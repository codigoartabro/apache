# The FROM instruction initializes a new build stage and sets the Base
# Image for subsequent instructions.
# As such, a valid Dockerfile must start with a FROM instruction.
FROM archlinux/base

# The LABEL instruction adds metadata to an image. A LABEL is a
# key-value pair. To include spaces within a LABEL value, use quotes
# and backslashes as you would in command-line parsing.
LABEL version="0.0.5"
LABEL description="Apache HTTPD + PHP + SSL"
LABEL maintainer="Laegnur"

# The EXPOSE instruction informs Docker that the container listens on
# the specified network ports at runtime. You can specify whether the
# port listens on TCP or UDP, and the default is TCP if the protocol is
# not specified.
# The EXPOSE instruction does not actually publish the port. It
# functions as a type of documentation between the person who builds
# the image and the person who runs the container, about which ports
# are intended to be published. To actually publish the port when
# running the container, use the -p flag on docker run to publish and
# map one or more ports, or the -P flag to publish all exposed ports
# and map them to high-order ports.
EXPOSE 80/tcp 443/tcp

# The ENV instruction sets the environment variable <key> to the value
# <value>.
# This value will be in the environment for all subsequent instructions
# in the build stage and can be replaced inline in many as well.

# The WORKDIR instruction sets the working directory for any RUN, CMD,
# ENTRYPOINT, COPY and ADD instructions that follow it in the
# Dockerfile. If the WORKDIR doesn’t exist, it will be created even if
# it’s not used in any subsequent Dockerfile instruction.
# The WORKDIR instruction can be used multiple times in a Dockerfile.
# If a relative path is provided, it will be relative to the path of
# the previous WORKDIR instruction.

# The USER instruction sets the user name (or UID) and optionally the
# user group (or GID) to use when running the image and for any RUN,
# CMD and ENTRYPOINT instructions that follow it in the Dockerfile.

# The VOLUME instruction creates a mount point with the specified name
# and marks it as holding externally mounted volumes from native host
# or other containers

VOLUME /srv/http /var/log/httpd

# The RUN instruction will execute any commands in a new layer on top
# of the current image and commit the results.
# The resulting committed image will be used for the next step in the
# Dockerfile.

# Install packages.
RUN pacman --noconfirm -Sy grep apache php php-apache php-gd


# The ADD instruction copies new files, directories or remote file URLs
# from <src> and adds them to the filesystem of the image at the path
# <dest>.
# Multiple <src> resources may be specified but if they are files or
# directories, their paths are interpreted as relative to the source of
# the context of the build.

# The COPY instruction copies new files or directories from <src> and
# adds them to the filesystem of the container at the path <dest>.
# Multiple <src> resources may be specified but the paths of files and
# directories will be interpreted as relative to the source of the
# context of the build.
COPY config/httpd/conf /etc/httpd/conf/
COPY config/php/php.ini /etc/php/
COPY vhosts /srv/http/

RUN mkdir /etc/httpd/conf/vhosts-enabled
RUN ln -s /etc/httpd/conf/vhosts-available/*.conf /etc/httpd/conf/vhosts-enabled/

# The main purpose of a CMD is to provide defaults for an executing
# container.
# These defaults can include an executable, or they can omit the
# executable, in which case you must specify an ENTRYPOINT instruction
# as well.
CMD /usr/bin/httpd -k start -DFOREGROUND
